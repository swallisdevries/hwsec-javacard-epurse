package epurse.card;

public interface TerminalType {
	byte
			RELOAD  = 0,
			PAYMENT = 1;
}
