package epurse.card;

interface State {
	byte
			INITIAL                   = 0,
			HANDSHAKE_SENT_KEY        = 1,
			/** Mutually authenticated with reload terminal */
			MUT_AUTHENTICATED_RELOAD  = 2,
			/** Mutually authenticated with payment terminal */
			MUT_AUTHENTICATED_PAYMENT = 3,
			RELOAD_GAVE_BALANCE       = 4;
}
