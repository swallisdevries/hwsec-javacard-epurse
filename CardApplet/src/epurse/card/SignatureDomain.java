package epurse.card;

public interface SignatureDomain {
	// Signed by card issuer
	byte
			CERT_RELOAD  = TerminalType.RELOAD,
			CERT_PAYMENT = TerminalType.PAYMENT,
			CERT_CARD    = 2;

	// Signed by terminal / card
	byte
			SIGN_HANDSHAKE       = 0,
			SIGN_PAYMENT_RECEIPT = 1;
}
