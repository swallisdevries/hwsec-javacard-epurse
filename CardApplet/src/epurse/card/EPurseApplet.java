package epurse.card;

import javacard.framework.*;
import javacard.security.*;

/**
 * @author Steven Wallis de Vries
 * @author Lars Kuipers
 */
public class EPurseApplet extends Applet {
	/** APDU CLA byte for messages to applet */
	public static final byte CLASS = (byte) 0xB0;

	private static final byte
			idLength            = 4,
			signatureLength     = 1 + 72, // Signature length including length field
			publicKeyLength     = 65,
			secretKeyLength     = 32,
			macLength           = 32,
			terminalNonceLength = 16,
			cardNonceLength     = 4,
			balanceLength       = 2;

	/** Maximum balance in cents */
	private static final short maxBalance = 250_00;

	// ===== EEPROM (persistent) fields =====
	private       boolean     initialized;
	private       boolean     revoked;
	/** Balance in cents */
	private       short       balance;
	private final ECPublicKey issuerPublicKey         = (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, KeyBuilder.LENGTH_EC_FP_256, false);
	private final Signature   issuerSignatureVerifier = Signature.getInstance(Signature.ALG_ECDSA_SHA /*SHA due to limitation*/, false);
	private final KeyPair     cardKeyPair             = new KeyPair(KeyPair.ALG_EC_FP, KeyBuilder.LENGTH_EC_FP_256);
	private final Signature   signer                  = Signature.getInstance(Signature.ALG_ECDSA_SHA /*SHA due to limitation*/, false);
	private final byte[]      cardId                  = new byte[idLength];
	private final byte[]      publicKeySignature      = new byte[signatureLength];
	private final short[]     handshakeCardNonce      = new short[2], paymentCardNonce = new short[2];

	// ===== EEPROM fields but could be transient if JavaCard/jCardSim supported it =====
	private final ECPublicKey   terminalPublicKey         = (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, KeyBuilder.LENGTH_EC_FP_256, false);
	private final KeyPair       dhKeyPair                 = new KeyPair(KeyPair.ALG_EC_FP, KeyBuilder.LENGTH_EC_FP_256);
	private final Signature     terminalSignatureVerifier = Signature.getInstance(Signature.ALG_ECDSA_SHA /*SHA due to limitation*/, false);
	private final KeyAgreement  keyAgreement              = KeyAgreement.getInstance(KeyAgreement.ALG_EC_SVDP_DH /*SHA due to limitation*/, false);
	private final Signature     macMaker                  = Signature.getInstance(Signature.ALG_HMAC_SHA_256, false);
	private final Signature     macVerifier               = Signature.getInstance(Signature.ALG_HMAC_SHA_256, false);
	private final MessageDigest hash512                   = MessageDigest.getInstance(MessageDigest.ALG_SHA_512, false);

	// ===== Transient (per session) fields =====
	/** Scratch space used locally, could be local if JavaCard supported arrays on the stack */
	private final byte[]  scratch      = JCSystem.makeTransientByteArray((short) 65, JCSystem.CLEAR_ON_RESET);
	/** See {@link State} */
	private final byte[]  state        = JCSystem.makeTransientByteArray((short) 1, JCSystem.CLEAR_ON_RESET);
	/** See {@link TerminalType} */
	private final byte[]  terminalType = JCSystem.makeTransientByteArray((short) 1, JCSystem.CLEAR_ON_RESET);
	private final byte[]  terminalId   = JCSystem.makeTransientByteArray(idLength, JCSystem.CLEAR_ON_RESET);
	private final HMACKey macKey       = (HMACKey) KeyBuilder.buildKey(KeyBuilder.TYPE_HMAC_TRANSIENT_RESET, MessageDigest.LENGTH_SHA_512, false);

	/** Install applet (and allocate EEPROM), called first by JCRE */
	public static void install(byte[] buffer, short offset, byte length) {
		new EPurseApplet().register();
	}

	public boolean select() {
		return true;
	}

	public void process(APDU apdu) throws ISOException, APDUException {
		// Ignore the APDU that selects this applet
		if (selectingApplet()) return;

		try {
			byte[] buffer = apdu.getBuffer();

			if (revoked) {
				eraseInformation();
				ISOException.throwIt(Status.REVOKED);
			}

			if (buffer[ISO7816.CLA_ISO7816] != CLASS)
				ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);

			byte instruction = buffer[ISO7816.OFFSET_INS];

			if (instruction == Instruction.INITIALIZE) {
				if (initialized) throwCondNotSatisfied();
				initialize(apdu);

			} else {
				if (!initialized) throwCondNotSatisfied();

				switch (instruction) {
					case Instruction.HANDSHAKE:
						handshake(apdu);
						break;

					case Instruction.REVOKE:
						receiveRevoke(apdu);
						break;

					case Instruction.GIVE_BALANCE:
						giveBalance(apdu);
						break;
					case Instruction.RELOAD:
						reload(apdu);
						break;

					case Instruction.PAY:
						pay(apdu);
						break;

					default:
						ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
				}
			}
		} catch (Throwable e) {
			// Go back to initial state on failure
			state[0] = State.INITIAL;
			throw e;
		}
	}


	private void throwCondNotSatisfied() throws ISOException {
		ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
	}

	private void checkParametersZero(byte[] buffer) throws ISOException {
		if (Util.getShort(buffer, ISO7816.OFFSET_P1) != 0) ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
	}

	/**
	 * Erase private key and clear balance
	 */
	private void eraseInformation() {
		cardKeyPair.getPrivate().clearKey();
		balance = 0;
	}

	/**
	 * Immediately revoke card, clear state, erase information
	 * <p>Does not throw
	 */
	private void revoke() {
		revoked = true;
		state[0] = State.INITIAL;
		eraseInformation();
	}

	/**
	 * Try to to increment nonce and write to destBuffer,
	 * revoke card and throw otherwise
	 * <p>Uses big endian
	 *
	 * @param nonce Current nonce value
	 */
	private void writeNextNonce(short[] nonce, byte[] destBuffer, short destOffset) throws ISOException {
		if (nonce[1] == -1 && nonce[0] == -1) {
			revoke();
			ISOException.throwIt(Status.REVOKED);
		}

		if (nonce[1] == -1) {
			// Prevent nonce replay by card tear
			JCSystem.beginTransaction();
			nonce[1] = 0;
			++nonce[0];
			JCSystem.commitTransaction();
		} else ++nonce[1];

		Util.setShort(destBuffer, destOffset, nonce[0]);
		Util.setShort(destBuffer, (short) (destOffset + 2), nonce[1]);
	}

	/**
	 * Finalize signature with finalInput and check length + signature + 0-padding from signatureBuffer,
	 * throw if invalid
	 */
	private void checkSignature(Signature signer,
	                            byte[] finalInput, short finalInputOffset, short finalInputLength,
	                            byte[] signatureBuffer, short signatureOffset) throws ISOException {
		short length = (short) (signatureBuffer[signatureOffset] & 0xFF); // Make unsigned
		if (length > signatureLength - 1)
			ISOException.throwIt(ISO7816.SW_WRONG_DATA);
		if (!signer.verify(
				finalInput, finalInputOffset, finalInputLength,
				signatureBuffer, ((short) (signatureOffset + 1)), length))
			ISOException.throwIt(ISO7816.SW_WRONG_DATA);
	}

	/**
	 * Finalize signature with finalInput and add length + signature + 0-padding to destBuffer
	 */
	private void addSignature(Signature signer,
	                          byte[] finalInput, short finalInputOffset, short finalInputLength,
	                          byte[] destBuffer, short destOffset) {
		byte length = (byte) signer.sign(
				finalInput, finalInputOffset, finalInputLength,
				destBuffer, (short) (destOffset + 1)); // Will be positive
		destBuffer[destOffset] = length; // Prepend signature length
		// Pad with zeros
		for (short padOffset = (short) (destOffset + 1 + length);
		     padOffset < (short) (destOffset + signatureLength); ++padOffset)
			destBuffer[padOffset] = 0;
	}

	/**
	 * Verify MAC at the end of a C-APDU,
	 * throw if invalid
	 *
	 * <p>MAC is computed over: CLA, INS, P1, P2, Lc, Data excl MAC itself
	 */
	private void verifyMac(APDU apdu) throws ISOException {
		byte[] buffer = apdu.getBuffer();
		if (!macVerifier.verify(
				buffer, (short) 0, (short) (ISO7816.OFFSET_CDATA + apdu.getIncomingLength() - macLength),
				buffer, (short) (ISO7816.OFFSET_CDATA + apdu.getIncomingLength() - macLength), macLength))
			ISOException.throwIt(ISO7816.SW_WRONG_DATA);
	}

	/**
	 * Compute command part of R-APDU MAC using metadata of C-APDU
	 *
	 * <p>Command part of MAC is computed over: CLA, INS, P1, P2
	 *
	 * @param commandBuffer Buffer of C-APDU
	 * @return Partial Signature to be used to complete MAC using R-APDU
	 */
	private Signature startAddMac(byte[] commandBuffer) {
		macMaker.update(commandBuffer, (short) 0, ISO7816.OFFSET_LC);
		return macMaker;
	}

	/**
	 * Compute response part of R-APDU MAC, add to APDU, and send
	 *
	 * <p>Response part of MAC is computed over: response Data
	 *
	 * @param macMaker       Partial Signature from {@link EPurseApplet#startAddMac}
	 * @param response       R-APDU
	 * @param responseLength Length of response data excl MAC
	 */
	private void completeAddMacAndSend(Signature macMaker, APDU response, short responseLength) {
		byte[] responseBuffer = response.getBuffer();
		macMaker.sign(
				responseBuffer, (short) 0, responseLength,
				responseBuffer, responseLength);
		response.setOutgoingAndSend((short) 0, (short) (responseLength + macLength));
	}


	/**
	 * Process card initialization APDU by initialization terminal
	 *
	 * <p>Assumes: !initialized
	 * <p>Post: initialized
	 */
	private void initialize(APDU apdu) throws ISOException {
		/* C-APDU: INS: INITIALIZE; Data:
			- Issuer ECDSA public key
			- Card ECDSA secret key
			- Card ECDSA public key [1]
			- Card ID [2]
			- ECDSA signature by issuer on CERT_CARD, 1-2
			- Initial balance
		 */
		byte[] buffer = apdu.getBuffer();
		checkParametersZero(buffer);

		if (apdu.setIncomingAndReceive() !=
		    publicKeyLength + secretKeyLength + publicKeyLength
		    + idLength + signatureLength + balanceLength)
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);

		short offset = ISO7816.OFFSET_CDATA;

		issuerPublicKey.setW(buffer, offset, publicKeyLength);
		issuerSignatureVerifier.init(issuerPublicKey, Signature.MODE_VERIFY);
		offset += publicKeyLength;

		((ECPrivateKey) cardKeyPair.getPrivate()).setS(buffer, offset, secretKeyLength);
		signer.init(cardKeyPair.getPrivate(), Signature.MODE_SIGN);
		offset += secretKeyLength;
		((ECPublicKey) cardKeyPair.getPublic()).setW(buffer, offset, publicKeyLength);
		offset += publicKeyLength;

		Util.arrayCopy(
				buffer, offset,
				cardId, (short) 0, idLength);
		offset += idLength;

		// Signature assumed correct
		Util.arrayCopy(
				buffer, offset,
				publicKeySignature, (short) 0, signatureLength);
		offset += signatureLength;

		// Balance assumed correct
		balance = Util.getShort(buffer, offset);

		initialized = true;

		// R-APDU: OK
	}


	/**
	 * Process handshake APDU
	 *
	 * <p>Assumes: initialized, !revoked
	 * <p>State:
	 * <ul>
	 *  <li>INITIAL -> HANDSHAKE_SENT_KEY / INITIAL (nonce overflow)</li>
	 *  <li>HANDSHAKE_SENT_KEY -> MUT_AUTHENTICATED_*</li>
	 * </ul>
	 */
	private void handshake(APDU apdu) throws ISOException {
		byte[] buffer = apdu.getBuffer();

		switch (state[0]) {
			case State.INITIAL: {
				/* C-APDU: INS: HANDSHAKE, P1: 0, P2: terminal type; Data:
					- Terminal ECDSA public key [1]
					- Terminal ID [2]
					- ECDSA signature by issuer on CERT_[RELOAD/PAYMENT], 1-2
				 */

				// Check handshake sequence number
				if (buffer[ISO7816.OFFSET_P1] != 0) throwCondNotSatisfied();

				byte terminalType0 = buffer[ISO7816.OFFSET_P2];
				if (terminalType0 != TerminalType.RELOAD && terminalType0 != TerminalType.PAYMENT)
					ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);

				terminalType[0] = terminalType0;

				if (apdu.setIncomingAndReceive() != publicKeyLength + idLength + signatureLength)
					ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);

				short offset = ISO7816.OFFSET_CDATA;

				terminalPublicKey.setW(buffer, offset, publicKeyLength);
				terminalSignatureVerifier.init(terminalPublicKey, Signature.MODE_VERIFY);
				offset += publicKeyLength;

				Util.arrayCopy(
						buffer, offset,
						terminalId, (short) 0, idLength);
				offset += idLength;

				// offset: signature on terminal certificate
				{
					scratch[0] = terminalType0;
					issuerSignatureVerifier.update(scratch, (short) 0, (short) 1);

					checkSignature(issuerSignatureVerifier,
							buffer, ISO7816.OFFSET_CDATA /*terminalPublicKey & terminalId*/, (short) (publicKeyLength + idLength),
							buffer, offset /*signature*/);
				}


				/* R-APDU: OK;
					- Card ECDSA public key [1]
					- Card ID [2]
					- ECDSA signature by issuer on CERT_CARD, 1-2
					- Card nonce
				 */
				short responseLength = (short)
						(publicKeyLength + idLength + signatureLength + cardNonceLength);

				dhKeyPair.genKeyPair();
				keyAgreement.init(dhKeyPair.getPrivate());

				offset = 0;

				((ECPublicKey) cardKeyPair.getPublic()).getW(buffer, offset);
				offset += publicKeyLength;

				Util.arrayCopy(
						cardId, (short) 0,
						buffer, offset, idLength);
				offset += idLength;

				Util.arrayCopy(
						publicKeySignature, (short) 0,
						buffer, offset, signatureLength);
				offset += signatureLength;

				writeNextNonce(handshakeCardNonce, buffer, offset);

				state[0] = State.HANDSHAKE_SENT_KEY;

				apdu.setOutgoingAndSend((short) 0, responseLength);
				break;
			}


			case State.HANDSHAKE_SENT_KEY: {
				/* C-APDU: INS: HANDSHAKE, P1: 1; Data:
					- Terminal ECDHE public key [1]
					- ECDSA signature by terminal on SIGN_HANDSHAKE,
						ECDSA public key of card, card nonce, 1
					- Terminal nonce
	            */

				// Check handshake sequence number
				if (buffer[ISO7816.OFFSET_P1] != 1) throwCondNotSatisfied();

				if (buffer[ISO7816.OFFSET_P2] != 0) ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);

				if (apdu.setIncomingAndReceive() != publicKeyLength + signatureLength + terminalNonceLength)
					ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);

				short offset = ISO7816.OFFSET_CDATA;

				{
					// Generate raw shared ECDH secret
					short secretLength = keyAgreement.generateSecret(
							buffer, offset, publicKeyLength,
							scratch, (short) 0);
					offset += publicKeyLength;

					// Hash secret
					hash512.doFinal(
							scratch, (short) 0, secretLength,
							scratch, (short) 0);

					// Use secret as MAC key
					macKey.setKey(scratch, (short) 0, MessageDigest.LENGTH_SHA_512);
					macMaker.init(macKey, Signature.MODE_SIGN);
					macVerifier.init(macKey, Signature.MODE_VERIFY);
				}

				{
					scratch[0] = SignatureDomain.SIGN_HANDSHAKE;
					terminalSignatureVerifier.update(scratch, (short) 0, (short) 1);

					// Include ECDSA public key of card
					((ECPublicKey) cardKeyPair.getPublic()).getW(scratch, (short) 0);
					terminalSignatureVerifier.update(scratch, (short) 0, publicKeyLength);

					// Include card nonce
					Util.setShort(scratch, (short) 0, handshakeCardNonce[0]);
					Util.setShort(scratch, (short) 2, handshakeCardNonce[1]);
					terminalSignatureVerifier.update(scratch, (short) 0, cardNonceLength);

					checkSignature(terminalSignatureVerifier,
							buffer, ISO7816.OFFSET_CDATA /*terminal ECDH public key*/, publicKeyLength,
							buffer, offset /*signature*/);
				}

				state[0] = terminalType[0] == TerminalType.RELOAD
				           ? State.MUT_AUTHENTICATED_RELOAD
				           : State.MUT_AUTHENTICATED_PAYMENT;

				/* R-APDU: OK;
					- Card ECDHE public key [1]
					- ECDSA signature by card on SIGN_HANDSHAKE,
					    ECDSA public key of terminal, terminal nonce, 1
				 */
				short responseLength = (short) (publicKeyLength + signatureLength);

				offset = 0;

				// Note: this should not and do not overwrite the input data (terminal nonce) for the signature below
				((ECPublicKey) dhKeyPair.getPublic()).getW(buffer, offset);
				offset += publicKeyLength;

				{
					scratch[0] = SignatureDomain.SIGN_HANDSHAKE;
					signer.update(scratch, (short) 0, (short) 1);

					terminalPublicKey.getW(scratch, (short) 0);
					signer.update(scratch, (short) 0, publicKeyLength);

					signer.update(buffer, (short) (ISO7816.OFFSET_CDATA + publicKeyLength + signatureLength) /*terminal nonce*/, terminalNonceLength);

					addSignature(signer,
							buffer, (short) 0 /*card ECDHE public key*/, publicKeyLength,
							buffer, offset);
				}

				apdu.setOutgoingAndSend((short) 0, responseLength);
				break;
			}


			default:
				throwCondNotSatisfied(); // We are not in the handshake phase
		}
	}


	/**
	 * Process revocation APDU
	 *
	 * <p>Assumes: initialized
	 * <p>State:
	 * <ul>
	 *  <li>MUT_AUTHENTICATED_* -> INITIAL</li>
	 * </ul>
	 */
	private void receiveRevoke(APDU apdu) throws ISOException {
		if (state[0] != State.MUT_AUTHENTICATED_RELOAD && state[0] != State.MUT_AUTHENTICATED_PAYMENT)
			throwCondNotSatisfied();

		// C-APDU: INS: REVOKE; Data: MAC
		byte[] buffer = apdu.getBuffer();
		checkParametersZero(buffer);

		if (apdu.setIncomingAndReceive() != macLength)
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);

		verifyMac(apdu);

		revoke();

		// R-APDU: OK; MAC
		completeAddMacAndSend(startAddMac(buffer), apdu, (short) 0);
	}


	/**
	 * Process balance request APDU by reload terminal
	 *
	 * <p>Assumes: initialized, !revoked
	 * <p>State:
	 * <ul>
	 *  <li>MUT_AUTHENTICATED_RELOAD -> RELOAD_GAVE_BALANCE</li>
	 * </ul>
	 */
	private void giveBalance(APDU apdu) throws ISOException {
		if (state[0] != State.MUT_AUTHENTICATED_RELOAD) throwCondNotSatisfied();

		// C-APDU: INS: GIVE_BALANCE; Data: MAC
		byte[] buffer = apdu.getBuffer();
		checkParametersZero(buffer);

		if (apdu.setIncomingAndReceive() != macLength)
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);

		verifyMac(apdu);

		state[0] = State.RELOAD_GAVE_BALANCE; // Prevent replay

		// R-APDU: OK; card balance, MAC
		Signature macMaker = startAddMac(buffer);
		Util.setShort(buffer, (short) 0, balance);
		completeAddMacAndSend(macMaker, apdu, balanceLength);
	}


	/**
	 * Process reload APDU by reload terminal
	 *
	 * <p>Assumes: initialized, !revoked
	 * <p>State:
	 * <ul>
	 *  <li>RELOAD_GAVE_BALANCE -> INITIAL</li>
	 * </ul>
	 */
	private void reload(APDU apdu) throws ISOException {
		if (state[0] != State.RELOAD_GAVE_BALANCE) throwCondNotSatisfied();

		// C-APDU: INS: RELOAD; Data: increase balance amount, MAC
		byte[] buffer = apdu.getBuffer();
		checkParametersZero(buffer);

		if (apdu.setIncomingAndReceive() != balanceLength + macLength)
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);

		verifyMac(apdu);

		short increaseAmount = Util.getShort(buffer, ISO7816.OFFSET_CDATA);
		if (increaseAmount < 0 || increaseAmount > (short) (maxBalance - balance))
			ISOException.throwIt(ISO7816.SW_WRONG_DATA);

		// Reload process done, return to initial state to prevent replay
		state[0] = State.INITIAL;

		balance += increaseAmount;

		// R-APDU: OK; MAC
		completeAddMacAndSend(startAddMac(buffer), apdu, (short) 0);
	}


	/**
	 * Process payment APDU by payment terminal
	 *
	 * <p>Assumes: initialized, !revoked
	 * <p>State:
	 * <ul>
	 *  <li>MUT_AUTHENTICATED_PAYMENT -> INITIAL</li>
	 * </ul>
	 */
	private void pay(APDU apdu) throws ISOException {
		if (state[0] != State.MUT_AUTHENTICATED_PAYMENT) throwCondNotSatisfied();

		/* C-APDU: INS: PAY; Data:
			- Payment amount
			- Terminal nonce
			- MAC
		 */
		byte[] buffer = apdu.getBuffer();
		checkParametersZero(buffer);

		if (apdu.setIncomingAndReceive() != balanceLength + terminalNonceLength + macLength)
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);

		verifyMac(apdu);

		// Payment process done, return to initial state to prevent replay
		state[0] = State.INITIAL;

		short paymentAmount = Util.getShort(buffer, ISO7816.OFFSET_CDATA);
		if (paymentAmount < 0) ISOException.throwIt(ISO7816.SW_WRONG_DATA);
		if (paymentAmount > balance) {
			// R-APDU: OK; sufficient balance = '0', MAC
			Signature macMaker = startAddMac(buffer);
			buffer[0] = 0;
			completeAddMacAndSend(macMaker, apdu, (short) 1);
			return;
		}

		balance -= paymentAmount;

		/* R-APDU: OK;
			- Sufficient balance = '1'
			- Card nonce [1]
			- ECDSA signature by card on SIGN_PAYMENT_RECEIPT,
				terminal nonce, terminal ID, 1, payment amount
		 */
		short responseLength = 1 + cardNonceLength + signatureLength;

		short offset = 0;

		// Note: these should not and do not overwrite the input data for the signature below

		buffer[0] = 1;
		++offset;

		writeNextNonce(paymentCardNonce, buffer, offset);
		offset += cardNonceLength;

		{
			scratch[0] = SignatureDomain.SIGN_PAYMENT_RECEIPT;
			signer.update(scratch, (short) 0, (short) 1);

			signer.update(buffer, (short) (ISO7816.OFFSET_CDATA + balanceLength) /*terminal nonce*/, terminalNonceLength);
			signer.update(terminalId, (short) 0, (short) terminalId.length);
			signer.update(buffer, (short) 1 /*paymentCardNonce*/, cardNonceLength);

			addSignature(signer,
					buffer, ISO7816.OFFSET_CDATA /*paymentAmount*/, balanceLength,
					buffer, offset);
		}

		apdu.setOutgoingAndSend((short) 0, responseLength);
	}
}
