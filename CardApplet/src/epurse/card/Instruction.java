package epurse.card;

public interface Instruction {
	byte
			INITIALIZE   = 0,
			HANDSHAKE    = 1,
			REVOKE       = 2,
			GIVE_BALANCE = 3,
			RELOAD       = 4,
			PAY          = 5;
}
