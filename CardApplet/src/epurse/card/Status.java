package epurse.card;

/**
 * Custom Status Words
 */
public interface Status {
	short
			REVOKED = 0x7000; // Card was revoked and cannot be used
}
