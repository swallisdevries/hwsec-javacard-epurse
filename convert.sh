#!/usr/bin/bash

. envvars.sh

echo appletPackage = $appletPackage
echo appletClassName = $appletClassName
echo appletSource = $appletSource
echo jCardSimJar = $jCardSimJar
echo appletOutDir = $appletOutDir
echo jcToolsHome = $jcToolsHome
echo packageAid = $packageAid
echo classAid = $classAid
echo appletVersion = $appletVersion

# Certain Xlint options do crash because @Deprecated & @Override are not available

printf "\nCompiling applet...\n" &&
javac -g -Xlint:all,-deprecation,-overrides -source 7 -target 7 -bootclasspath "$jCardSimJar" $appletSource -d "$appletOutDir" &&
printf "\nConverting to CAP...\n" &&
java "-Djc.home=$jcToolsHome" -classpath "$jcToolsHome/lib/tools.jar" com.sun.javacard.converter.Main \
    -verbose -classdir "$appletOutDir" -applet $classAid $appletClassName \
    $appletPackage $packageAid $appletVersion

exit_code=$?

if [[ "$OSTYPE" == "msys" ]]; then
  read #pause
fi

exit $exit_code
