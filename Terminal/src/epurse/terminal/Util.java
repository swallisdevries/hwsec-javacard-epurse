package epurse.terminal;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class Util {
    public static byte[] packInt(int value) {
        byte[] buffer = new byte[4];

        buffer[0] = (byte)((value >> 24) & 0xff);
        buffer[1] = (byte)((value >> 16) & 0xff);
        buffer[2] = (byte)((value >>  8) & 0xff);
        buffer[3] = (byte)((value      ) & 0xff);
        
        return buffer;
    }

    public static void packInt(int value, byte[] dest, int offset) {
        dest[offset]   = (byte)((value >> 24) & 0xff);
        dest[offset+1] = (byte)((value >> 16) & 0xff);
        dest[offset+2] = (byte)((value >>  8) & 0xff);
        dest[offset+3] = (byte)((value      ) & 0xff);
    }

    public static int unpackInt(byte[] data, int offset) {
        return ((int)data[offset] << 24) | ((int)data[offset+1] << 16) | ((int)data[offset+2] << 8) | ((int)data[offset+3] & 0xff);
    }

    public static void packShort(short value, byte[] dest, int offset) {
        dest[offset] = (byte)(value >> 8);
        dest[offset+1] = (byte)(value & 0xff);
    }

    public static short unpackShort(byte[] data, int offset) {
        return (short) (((short)data[offset] << 8) | ((short)data[offset+1] & 0xff));
    }

    public static void packLong(long value, byte[] dest, int offset) {
        dest[offset]   = (byte)((value >> 56) & 0xff);
        dest[offset+1] = (byte)((value >> 48) & 0xff);
        dest[offset+2] = (byte)((value >> 40) & 0xff);
        dest[offset+3] = (byte)((value >> 32) & 0xff);
        dest[offset+4] = (byte)((value >> 24) & 0xff);
        dest[offset+5] = (byte)((value >> 16) & 0xff);
        dest[offset+6] = (byte)((value >>  8) & 0xff);
        dest[offset+7] = (byte)((value      ) & 0xff);
    }
}
