package epurse.terminal;

import com.licel.jcardsim.smartcardio.CardSimulator;
import epurse.card.EPurseApplet;
import epurse.card.TerminalType;
import epurse.terminal.terminals.ActivationTerminal;
import epurse.terminal.terminals.PaymentTerminal;
import epurse.terminal.terminals.ReloadTerminal;
import javacard.security.ECPublicKey;
import javacard.security.KeyPair;

import javax.smartcardio.CardException;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import static epurse.terminal.Constants.CALC_APPLET_AID;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class Main {
    private static final Scanner scanner = new Scanner(System.in);

    private static ReloadTerminal createReloadTerminal(CardSimulator simulator, KeyPair issuerKeyPair, int terminalId) {
        KeyPair keyPair = Crypto.generateKeyPair();
        byte[] certificate = Crypto.generateIssuerCertificate(issuerKeyPair, (ECPublicKey) keyPair.getPublic(), Util.packInt(terminalId), TerminalType.RELOAD);

        return new ReloadTerminal(simulator, issuerKeyPair.getPublic(), keyPair, terminalId, certificate);
    }

    private static PaymentTerminal createPaymentTerminal(CardSimulator simulator, KeyPair issuerKeyPair, int terminalId) {
        KeyPair keyPair = Crypto.generateKeyPair();
        byte[] certificate = Crypto.generateIssuerCertificate(issuerKeyPair, (ECPublicKey) keyPair.getPublic(), Util.packInt(terminalId), TerminalType.PAYMENT);

        return new PaymentTerminal(simulator, issuerKeyPair.getPublic(), keyPair, terminalId, certificate);
    }

    private static int getInt(String prompt, int min, int max) {
        boolean ok = false;
        int value = 0;

        while(!ok) {
            try {
                System.out.print(prompt);
                value = scanner.nextInt();

                if(value < min) {
                    System.err.printf("Value is too small, must be at least %d\n", min);
                } else if(value > max) {
                    System.err.printf("Value is too large, must be at most %d\n", max);
                } else {
                    ok = true;
                }
            } catch (InputMismatchException e) {
                System.err.printf("Could not parse input as integer: %s\n", e.getMessage());
                scanner.nextLine();
            }
        }

        return value;
    }

    private static void replActivationTerminal(ActivationTerminal terminal) {
        short balance = (short)getInt("Enter initial balance: ", Short.MIN_VALUE, Short.MAX_VALUE);

        try {
            terminal.connect();
            terminal.initializeCard(balance);
            System.out.printf("The card has been initialized with a balance of %d (€ %d.%02d)\n", balance, balance / 100, balance % 100);
            terminal.disconnect();
        } catch (CardException e) {
            System.err.printf("A CardException occurred: %s\n", e.getMessage());
        } catch (TerminalException e) {
            System.err.printf("A TerminalException occurred: %s\n", e.getMessage());
        }
    }

    private static void replReloadTerminal(ReloadTerminal terminal) {
        try {
            terminal.connect();

            short balance = terminal.askBalance();
            System.out.printf("Your current card balance is %d (€ %d.%02d)\n", balance, balance / 100, balance % 100);

            short amount = (short)getInt("Enter reload amount: ", Short.MIN_VALUE, Short.MAX_VALUE);
            short newBalance = (short)(balance + amount);
            terminal.reload(amount);
            System.out.printf("Your balance has been increased to %d (€ %d.%02d)\n", newBalance, newBalance / 100, newBalance % 100);

            terminal.disconnect();
        } catch (CardException e) {
            System.err.printf("A CardException occurred: %s\n", e.getMessage());
        } catch (TerminalException e) {
            System.err.printf("A TerminalException occurred: %s\n", e.getMessage());
        }
    }

    private static void replPaymentTerminal(PaymentTerminal terminal) {
        try {
            terminal.connect();

            short amount = (short)getInt("Enter payment amount: ", Short.MIN_VALUE, Short.MAX_VALUE);
            terminal.pay(amount).ifPresentOrElse(
                    System.out::println,
                    () -> System.out.println("Payment did not succeed (insufficient balance)")
            );

            terminal.disconnect();
        } catch (CardException e) {
            System.err.printf("A CardException occurred: %s\n", e.getMessage());
        } catch (TerminalException e) {
            System.err.printf("A TerminalException occurred: %s\n", e.getMessage());
        }
    }

    private static void repl(ActivationTerminal activationTerminal, List<ReloadTerminal> reloadTerminals, List<PaymentTerminal> paymentTerminals) {
        boolean exit = false;
        int terminalCount = 1 + reloadTerminals.size() + paymentTerminals.size();

        while(!exit) {
            System.out.println("0) Exit");
            System.out.println("1) Connect to activation terminal");
            for(int i = 0; i < reloadTerminals.size(); ++i) {
                System.out.printf("%d) Connect to reload terminal (ID=%d)\n", i+2, reloadTerminals.get(i).getTerminalId());
            }
            for(int i = 0; i < paymentTerminals.size(); ++i) {
                System.out.printf("%d) Connect to payment terminal (ID=%d)\n", i+2+reloadTerminals.size(), paymentTerminals.get(i).getTerminalId());
            }
            int action = getInt("Please select an action: ", 0, terminalCount);

            if(action == 0) {
                exit = true;
            } else if(action == 1) {
                replActivationTerminal(activationTerminal);
            } else if(action - 2 < reloadTerminals.size()) {
                ReloadTerminal terminal = reloadTerminals.get(action - 2);
                replReloadTerminal(terminal);
            } else {
                PaymentTerminal terminal = paymentTerminals.get(action - 2 - reloadTerminals.size());
                replPaymentTerminal(terminal);
            }
        }
    }

    private static void runTest(ActivationTerminal activationTerminal, ReloadTerminal reloadTerminal, PaymentTerminal paymentTerminal1, PaymentTerminal paymentTerminal2) throws CardException, TerminalException {
        activationTerminal.connect();
        activationTerminal.initializeCard((short)68);
        activationTerminal.disconnect();

        reloadTerminal.connect();
        System.out.printf("The current card balance is: %d\n", reloadTerminal.askBalance());
        reloadTerminal.reload((short)32);
        System.out.println("Increased card balance");
        reloadTerminal.disconnect();

        reloadTerminal.connect();
        System.out.printf("The current card balance is: %d\n", reloadTerminal.askBalance());
        reloadTerminal.disconnect();

        paymentTerminal1.connect();
        paymentTerminal1.pay((short)82)
                .ifPresentOrElse(
                        System.out::println,
                        () -> System.out.println("Payment did not succeed")
                );
        paymentTerminal1.disconnect();

        reloadTerminal.connect();
        System.out.printf("The current card balance is: %d\n", reloadTerminal.askBalance());
        reloadTerminal.disconnect();

        paymentTerminal2.connect();
        paymentTerminal2.pay((short)15)
                .ifPresentOrElse(
                        System.out::println,
                        () -> System.out.println("Payment did not succeed")
                );
        paymentTerminal2.disconnect();

        reloadTerminal.connect();
        System.out.printf("The current card balance is: %d\n", reloadTerminal.askBalance());
        reloadTerminal.disconnect();

        paymentTerminal2.connect();
        paymentTerminal2.pay((short)4)
                .ifPresentOrElse(
                        System.out::println,
                        () -> System.out.println("Payment did not succeed")
                );
        paymentTerminal2.disconnect();
    }

    public static void main(String[] args) throws CardException, TerminalException {
        CardSimulator simulator = new CardSimulator();
        simulator.installApplet(CALC_APPLET_AID, EPurseApplet.class);
        KeyPair issuerKeyPair = Crypto.generateKeyPair();

        ActivationTerminal activationTerminal = new ActivationTerminal(simulator, issuerKeyPair, 108);
        ReloadTerminal reloadTerminal = createReloadTerminal(simulator, issuerKeyPair, 48);
        PaymentTerminal paymentTerminal1 = createPaymentTerminal(simulator, issuerKeyPair, 1516);
        PaymentTerminal paymentTerminal2 = createPaymentTerminal(simulator, issuerKeyPair, 2342);

        repl(activationTerminal, List.of(reloadTerminal), List.of(paymentTerminal1, paymentTerminal2));
        //runTest(activationTerminal, reloadTerminal, paymentTerminal1, paymentTerminal2);
    }
}
