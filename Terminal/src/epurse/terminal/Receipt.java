package epurse.terminal;

import javacard.security.PublicKey;

import java.math.BigInteger;
import java.time.Instant;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class Receipt {
    private final int cardId;
    private final PublicKey cardKey;
    private final byte[] cardNonce;
    private final byte[] cardSignature;
    private final int terminalId;
    private final PublicKey terminalKey;
    private final byte[] terminalNonce;
    private final byte[] terminalSignature;
    private final short amount;
    private final Instant instant;

    public Receipt(int cardId, PublicKey cardKey, byte[] cardNonce, byte[] cardSignature,
                   int terminalId, PublicKey terminalKey, byte[] terminalNonce, byte[] terminalSignature,
                   short amount, Instant instant) {
        this.cardId = cardId;
        this.cardKey = cardKey;
        this.cardNonce = cardNonce;
        this.cardSignature = cardSignature;
        this.terminalId = terminalId;
        this.terminalKey = terminalKey;
        this.terminalNonce = terminalNonce;
        this.terminalSignature = terminalSignature;
        this.amount = amount;
        this.instant = instant;
    }

    @Override
    public String toString() {
        BigInteger cardTransactionId = new BigInteger(cardNonce);
        BigInteger terminalTransactionId = new BigInteger(terminalNonce);

        return "=============================================\n" +
                "============== Payment receipt ==============\n" +
                "date:   " + instant + '\n' +
                "amount: " + amount + '\n' +
                "================= Card Info =================\n" +
                "Identifier:  " + cardId + '\n' +
                "Transaction: " + cardTransactionId + '\n' +
                "Public key:  " + Crypto.fingerprint(cardKey) + '\n' +
                "Signature:   " + Crypto.fingerprint(cardSignature) + '\n' +
                "=============== Terminal Info ===============\n" +
                "Identifier:  " + terminalId + '\n' +
                "Transaction: " + terminalTransactionId + '\n' +
                "Public key:  " + Crypto.fingerprint(terminalKey) + '\n' +
                "Signature:   " + Crypto.fingerprint(terminalSignature) + '\n' +
                "=============================================\n";
    }
}
