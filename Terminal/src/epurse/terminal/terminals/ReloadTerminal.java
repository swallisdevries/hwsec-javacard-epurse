package epurse.terminal.terminals;

import com.licel.jcardsim.smartcardio.CardSimulator;
import epurse.card.Instruction;
import epurse.card.TerminalType;
import epurse.terminal.CommandAPDUBuilder;
import epurse.terminal.Crypto;
import epurse.terminal.TerminalException;
import epurse.terminal.Util;
import javacard.security.KeyPair;
import javacard.security.PublicKey;

import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import static epurse.terminal.Constants.*;

/**
 * The authenticated terminal used to increase the balance on cards.
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class ReloadTerminal extends AuthenticatedTerminal {
    /**
     * Creates a new reload terminal.
     * @param simulator The simulator instance.
     * @param issuerKey The public key of the issuer, used to verify issued key pair certificates.
     * @param terminalKeyPair The key pair of this terminal.
     * @param terminalId The identifier of this terminal.
     * @param terminalCertificate The certificate of this terminal, signed by the issuer.
     */
    public ReloadTerminal(CardSimulator simulator, PublicKey issuerKey, KeyPair terminalKeyPair, int terminalId, byte[] terminalCertificate) {
        super(simulator, "ReloadTerminal-" + terminalId, issuerKey, terminalKeyPair, TerminalType.RELOAD, terminalId, terminalCertificate);
    }

    /**
     * Queries the card for the current balance.
     * @return The card balance.
     * @throws CardException if the card operation failed
     * @throws TerminalException if the card returned an unexpected response, or the MAC failed to verify
     */
    public short askBalance() throws CardException, TerminalException {
        // C-APDU: INS: GIVE_BALANCE; Data: MAC
        CommandAPDU commandAPDU = new CommandAPDUBuilder(Instruction.GIVE_BALANCE, BALANCE_RESPONSE_APDU_LENGTH)
                .setExpectedLength(BALANCE_COMMAND_APDU_LENGTH)
                .addMAC(macKey)
                .build();
        ResponseAPDU responseAPDU = channel.transmit(commandAPDU);
        checkSW(responseAPDU, BALANCE_RESPONSE_APDU_LENGTH);

        // R-APDU: OK; card balance, MAC
        byte[] data = responseAPDU.getData();

        if(!Crypto.verifyMAC(macKey, Instruction.GIVE_BALANCE, data)) {
            throw new TerminalException("Failed to verify MAC on GIVE_BALANCE");
        }

        return Util.unpackShort(data, 0);
    }

    /**
     * Reloads the card by increasing the balance with the provided amount.
     * @param balance The balance amount to increase the card balance with. This amount cannot be negative, nor may the
     *                new balance exceed the maximum card balance.
     * @apiNote {@link #askBalance()} must be called prior to calling this method.
     * @throws CardException if the card operation failed
     * @throws TerminalException if the card returned an unexpected response, or the MAC failed to verify
     */
    public void reload(short balance) throws CardException, TerminalException {
        // C-APDU: INS: RELOAD; Data: increase balance amount, MAC
        CommandAPDU commandAPDU = new CommandAPDUBuilder(Instruction.RELOAD, RELOAD_RESPONSE_APDU_LENGTH)
                .setExpectedLength(RELOAD_COMMAND_APDU_LENGTH)
                .add(balance)
                .addMAC(macKey)
                .build();
        ResponseAPDU responseAPDU = channel.transmit(commandAPDU);
        checkSW(responseAPDU, RELOAD_RESPONSE_APDU_LENGTH);

        // R-APDU: OK; MAC
        if(!Crypto.verifyMAC(macKey, Instruction.RELOAD, responseAPDU.getData())) {
            throw new TerminalException("Failed to verify MAC on RELOAD");
        }
    }

}
