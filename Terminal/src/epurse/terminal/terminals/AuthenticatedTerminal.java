package epurse.terminal.terminals;

import com.licel.jcardsim.smartcardio.CardSimulator;
import epurse.card.Instruction;
import epurse.card.SignatureDomain;
import epurse.terminal.*;
import javacard.security.*;

import javax.smartcardio.*;
import java.util.Arrays;
import java.util.List;

import static epurse.terminal.Constants.*;

/**
 * The base class for all terminals which require the mutual authentication handshake to be performed after connecting
 * the card before prior operations can occur.
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public abstract class AuthenticatedTerminal extends Terminal {
	private final PublicKey issuerKey;
	protected final KeyPair terminalKeyPair;
	private final byte terminalType;
	protected final int terminalId;
	private final byte[] terminalCertificate;
	protected int cardId;
	protected final ECPublicKey cardPublicKey;
	protected final HMACKey macKey;

	/**
	 * Creates a new authenticated terminal.
	 * @param simulator The simulator instance.
	 * @param terminalName The name of this terminal.
	 * @param issuerKey The public key of the issuer, used to verify issued key pair certificates.
	 * @param terminalKeyPair The key pair of this terminal.
	 * @param terminalId The identifier of this terminal.
	 * @param terminalCertificate The certificate of this terminal, signed by the issuer.
	 * @param terminalType The type of this terminal, as defined in {@link epurse.card.TerminalType}.
	 */
	public AuthenticatedTerminal(CardSimulator simulator, String terminalName, PublicKey issuerKey, KeyPair terminalKeyPair, byte terminalType, int terminalId, byte[] terminalCertificate) {
		super(simulator, terminalName);

		this.issuerKey = issuerKey;
		this.terminalKeyPair = terminalKeyPair;
		this.terminalType = terminalType;
		this.terminalId = terminalId;
		this.terminalCertificate = terminalCertificate;
		this.cardId = 0;
		this.cardPublicKey = (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, KeyBuilder.LENGTH_EC_FP_256, false);
		this.macKey = (HMACKey) KeyBuilder.buildKey(KeyBuilder.TYPE_HMAC_TRANSIENT_RESET, MessageDigest.LENGTH_SHA_512, false);
	}

	private boolean isCardOnRevocationList() {
		// Very simple mock-up of the revocation list, which is just a static list of card identifiers to revoke for
		// testing purposes.
		List<Integer> revokedCardIds = List.of(1);

		return revokedCardIds.contains(cardId);
	}

	/**
	 * Connects the card to the terminal, and then performs the mutual authentication handshake. Once the handshake
	 * completes, a shared session key is established which is used for MACs. If the card is on the to be revoked list,
	 * the card is revoked after the handshake completes.
	 * @throws CardException if the card operation failed
	 * @throws TerminalException if the card returned an unexpected response, a signature failed to verify, or selecting
	 * the applet failed
	 */
	@Override
	public void connect() throws CardException, TerminalException {
		super.connect();

		handshake();

		// If the card is on the revocation list, then revoke it once it has been authenticated. This should prevent an
		// attacker from spoofing/altering messages with the goal of getting a valid card to be revoked as a denial of
		// service attack.
		if(isCardOnRevocationList()) {
			revoke();
		}
	}

	private void handshake() throws CardException, TerminalException {
		/* C-APDU: INS: HANDSHAKE, P1: 0, P2: terminal type; Data:
			- Terminal ECDSA public key [1]
			- Terminal ID [2]
			- ECDSA signature by issuer on CERT_[RELOAD/PAYMENT], 1-2
		*/
		CommandAPDU commandAPDU = new CommandAPDUBuilder(Instruction.HANDSHAKE, HS_1_RESPONSE_APDU_LENGTH, 0, terminalType)
				.setExpectedLength(HS_1_COMMAND_APDU_LENGTH)
				.add(terminalKeyPair.getPublic())
				.add(terminalId)
				.add(terminalCertificate)
				.build();
		ResponseAPDU responseAPDU = channel.transmit(commandAPDU);
		checkSW(responseAPDU, HS_1_RESPONSE_APDU_LENGTH);

		/* R-APDU: OK;
			- Card ECDSA public key [1]
			- Card ID [2]
			- ECDSA signature by issuer on CERT_CARD, 1-2
			- Card nonce
		*/
		byte[] data = responseAPDU.getData();
		short offset = 0;

		cardPublicKey.setW(data, offset, (short) Crypto.PUBLIC_KEY_LENGTH);
		offset += Crypto.PUBLIC_KEY_LENGTH;
		cardId = Util.unpackInt(data, offset);
		offset += ID_LENGTH;
		byte[] cardCertificate = Arrays.copyOfRange(data, offset, offset + Crypto.SIGNATURE_LENGTH);
		offset += Crypto.SIGNATURE_LENGTH;
		byte[] cardNonce = Arrays.copyOfRange(data, offset, offset + CARD_NONCE_LENGTH);

		if(!Crypto.verifySignature(issuerKey, data, 0, Crypto.PUBLIC_KEY_LENGTH + ID_LENGTH, cardCertificate, SignatureDomain.CERT_CARD)) {
			throw new TerminalException("Failed to verify card certificate");
		}

		/* C-APDU: INS: HANDSHAKE, P1: 1; Data:
			- Terminal ECDHE public key [1]
			- ECDSA signature by terminal on SIGN_HANDSHAKE,
				ECDSA public key of card, card nonce, ECDHE public key [1]
			- Terminal nonce
		*/
		KeyPair dhKeyPair = Crypto.generateKeyPair();
		byte[] signature = new SignatureBuilder()
				.setExpectedLength(Crypto.PUBLIC_KEY_LENGTH + CARD_NONCE_LENGTH + Crypto.PUBLIC_KEY_LENGTH)
				.add(cardPublicKey)
				.add(cardNonce)
				.add(dhKeyPair.getPublic())
				.build(terminalKeyPair.getPrivate(), SignatureDomain.SIGN_HANDSHAKE);
		byte[] terminalNonce = Crypto.getNonce(TERMINAL_NONCE_LENGTH);

		commandAPDU = new CommandAPDUBuilder(Instruction.HANDSHAKE, HS_2_RESPONSE_APDU_LENGTH, 1, 0)
				.setExpectedLength(HS_2_COMMAND_APDU_LENGTH)
				.add(dhKeyPair.getPublic())
				.add(signature)
				.add(terminalNonce)
				.build();
		responseAPDU = channel.transmit(commandAPDU);
		checkSW(responseAPDU, HS_2_RESPONSE_APDU_LENGTH);

		/* R-APDU: OK;
			- Card ECDHE public key [1]
			- ECDSA signature by card on SIGN_HANDSHAKE,
				ECDSA public key of terminal, terminal nonce, 1
		*/
		data = responseAPDU.getData();

		byte[] sharedSecret = Crypto.computeSharedSecret(dhKeyPair.getPrivate(), data);
		byte[] cardSignature = Arrays.copyOfRange(data, Crypto.PUBLIC_KEY_LENGTH, data.length);

		// verify ECDSA signature
		if(!new SignatureBuilder()
				.setExpectedLength(Crypto.PUBLIC_KEY_LENGTH + TERMINAL_NONCE_LENGTH + Crypto.PUBLIC_KEY_LENGTH)
				.add(terminalKeyPair.getPublic())
				.add(terminalNonce)
				.add(data, 0, Crypto.PUBLIC_KEY_LENGTH) // Card ECDHE public key
				.verify(cardPublicKey, SignatureDomain.SIGN_HANDSHAKE, cardSignature)
		) {
			throw new TerminalException("Failed to verify card ECDSA signature");
		}

		// set MAC key
		macKey.setKey(sharedSecret, (short)0, (short)sharedSecret.length);
	}

	private void revoke() throws CardException, TerminalException {
		// C-APDU: INS: REVOKE; Data: MAC
		CommandAPDU commandAPDU = new CommandAPDUBuilder(Instruction.REVOKE, REVOKE_RESPONSE_APDU_LENGTH)
				.setExpectedLength(REVOKE_COMMAND_APDU_LENGTH)
				.addMAC(macKey)
				.build();
		ResponseAPDU responseAPDU = channel.transmit(commandAPDU);
		checkSW(responseAPDU, Crypto.MAC_LENGTH);

		// R-APDU: OK; MAC
		if(!Crypto.verifyMAC(macKey, Instruction.REVOKE, responseAPDU.getData())) {
			throw new TerminalException("Failed to verify MAC on REVOKE");
		}
	}

	public int getTerminalId() {
		return terminalId;
	}
}
