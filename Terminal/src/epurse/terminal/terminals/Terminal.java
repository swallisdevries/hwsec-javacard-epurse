package epurse.terminal.terminals;

import com.licel.jcardsim.smartcardio.CardSimulator;
import com.licel.jcardsim.smartcardio.CardTerminalSimulator;
import com.licel.jcardsim.utils.AIDUtil;
import epurse.terminal.TerminalException;
import javacard.framework.ISO7816;

import javax.smartcardio.*;

import static epurse.terminal.Constants.CALC_APPLET_AID;

/**
 * The base class for all terminals which manages connecting and disconnecting cards.
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public abstract class Terminal {
    protected static final int LENGTH_DONT_CARE = -1;

    private final CardSimulator simulator;
    private final CardTerminal terminal;
    protected CardChannel channel;

    /**
     * Creates a new terminal instance.
     * @param simulator The simulator instance.
     * @param terminalName The name of this terminal.
     */
    public Terminal(CardSimulator simulator, String terminalName) {
        this.simulator = simulator;
        this.terminal = CardTerminalSimulator.terminal(simulator, terminalName);
        this.channel = null;
    }

    /**
     * Checks if the response APDU contains an error status word.
     * @param apdu The response APDU.
     * @throws TerminalException if the status word contains an error
     */
    protected static void checkSW(ResponseAPDU apdu) throws TerminalException {
        checkSW(apdu, LENGTH_DONT_CARE);
    }

    /**
     * Checks if the response APDU contains an error status word, or an unexpected amount of response data bytes.
     * @param apdu The response APDU.
     * @param expectedLength The expected number of response data bytes.
     * @throws TerminalException if the status word contains an error, or the data length does not match the expected
     * length
     */
    protected static void checkSW(ResponseAPDU apdu, int expectedLength) throws TerminalException {
        if ((short) apdu.getSW() != ISO7816.SW_NO_ERROR) {
            throw TerminalException.fromISOStatusWord((short)apdu.getSW());
        }
        if (expectedLength != LENGTH_DONT_CARE && apdu.getNr() != expectedLength ) {
            throw new TerminalException(String.format("Unexpected response body length (expected %d, but got %d)",
                    expectedLength, apdu.getNr()));
        }
    }

    /**
     * Connects the card to the terminal.
     * @throws CardException if the card operation failed
     * @throws TerminalException if selecting the applet failed
     */
    public void connect() throws CardException, TerminalException {
        simulator.assignToTerminal(terminal);
        channel = terminal.connect("*").getBasicChannel();
        checkSW(channel.transmit(new CommandAPDU(AIDUtil.select(CALC_APPLET_AID))));
    }

    /**
     * Disconnects the card from the terminal, but does not reset the card.
     * @throws CardException if the card operation failed
     */
    public void disconnect() throws CardException {
        channel.getCard().disconnect(false);
    }
}
