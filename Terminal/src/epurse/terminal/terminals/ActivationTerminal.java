package epurse.terminal.terminals;

import com.licel.jcardsim.smartcardio.CardSimulator;
import epurse.card.Instruction;
import epurse.card.SignatureDomain;
import epurse.terminal.CommandAPDUBuilder;
import epurse.terminal.Crypto;
import epurse.terminal.TerminalException;
import epurse.terminal.Util;
import javacard.security.ECPublicKey;
import javacard.security.KeyPair;

import javax.smartcardio.*;

import static epurse.terminal.Constants.*;

/**
 * The terminal used to activate newly created cards prior to use.
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class ActivationTerminal extends Terminal {
    private final KeyPair issuerKeyPair;
    private int nextCardId;

    /**
     * Creates a new activation terminal.
     * @param simulator The simulator instance.
     * @param issuerKeyPair The key pair used to sign generated card key pairs.
     * @param nextCardId The next card identifier to use.
     */
    public ActivationTerminal(CardSimulator simulator, KeyPair issuerKeyPair, int nextCardId) {
        super(simulator, "ActivationTerminal");

        this.issuerKeyPair = issuerKeyPair;
        this.nextCardId = nextCardId;
    }

    /**
     * Initializes the connected card by generating a fresh key pair, which is loaded onto the card, along with the
     * other data.
     * @param balance The initial balance of the card.
     * @throws CardException if the card operation failed
     * @throws TerminalException if the card returned an unexpected response
     */
    public void initializeCard(short balance) throws CardException, TerminalException {
        KeyPair cardKeyPair = Crypto.generateKeyPair();
        byte[] cardIdPacked = Util.packInt(nextCardId);
        byte[] certificate = Crypto.generateIssuerCertificate(issuerKeyPair, (ECPublicKey) cardKeyPair.getPublic(), cardIdPacked, SignatureDomain.CERT_CARD);

        CommandAPDU commandAPDU = new CommandAPDUBuilder(Instruction.INITIALIZE, 0)
                .setExpectedLength(INIT_COMMAND_APDU_LENGTH)
                .add(issuerKeyPair.getPublic())
                .add(cardKeyPair.getPrivate())
                .add(cardKeyPair.getPublic())
                .add(nextCardId)
                .add(certificate)
                .add(balance)
                .build();
        ResponseAPDU responseAPDU = channel.transmit(commandAPDU);
        checkSW(responseAPDU);

        ++nextCardId;
    }
}
