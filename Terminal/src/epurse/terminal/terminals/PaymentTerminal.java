package epurse.terminal.terminals;

import com.licel.jcardsim.smartcardio.CardSimulator;
import epurse.card.Instruction;
import epurse.card.SignatureDomain;
import epurse.card.TerminalType;
import epurse.terminal.*;
import javacard.security.KeyPair;
import javacard.security.PublicKey;

import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

import static epurse.terminal.Constants.*;

/**
 * The authenticated terminal serving as a Point of Sale terminal for cards.
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class PaymentTerminal extends AuthenticatedTerminal {
    private final byte[] transactionId = new byte[TERMINAL_NONCE_LENGTH];

    /**
     * Creates a new payment terminal.
     * @param simulator The simulator instance.
     * @param issuerKey The public key of the issuer, used to verify issued key pair certificates.
     * @param terminalKeyPair The key pair of this terminal.
     * @param terminalId The identifier of this terminal.
     * @param terminalCertificate The certificate of this terminal, signed by the issuer.
     */
    public PaymentTerminal(CardSimulator simulator, PublicKey issuerKey, KeyPair terminalKeyPair, int terminalId, byte[] terminalCertificate) {
        super(simulator, "PaymentTerminal-" + terminalId, issuerKey, terminalKeyPair, TerminalType.PAYMENT, terminalId, terminalCertificate);
    }

    private void checkDataLength(ResponseAPDU responseAPDU) throws TerminalException {
        byte[] data = responseAPDU.getData();

        if(data.length == 0) {
            throw new TerminalException("Unexpected empty response body");
        } else if(data[0] == 1) {
            if(data.length != PAYMENT_RESPONSE_OK_APDU_LENGTH) {
                throw new TerminalException(String.format("Unexpected response body length (expected %d, but got %d)",
                        PAYMENT_RESPONSE_OK_APDU_LENGTH, data.length));
            }
        } else if(data[0] == 0) {
            if(data.length != PAYMENT_RESPONSE_FAIL_APDU_LENGTH) {
                throw new TerminalException(String.format("Unexpected response body length (expected %d, but got %d)",
                        PAYMENT_RESPONSE_FAIL_APDU_LENGTH, data.length));
            }
        } else {
            throw new TerminalException(String.format(
                    "Unexpected initial response body byte (expected 0 or 1, but got %d)", data[0]));
        }
    }

    private void incrementTransactionId() {
        // Makeshift BigInteger increment by 1 on fixed bit size, wrapping around on overflow.
        // Note that BigInteger uses a big endian byte order.
        int i = TERMINAL_NONCE_LENGTH;

        do {
            transactionId[--i]++;
        } while (transactionId[i] == 0 && i > 0);
    }

    /**
     * Attempts to make a payment of the given amount between this Point of Sale terminal and the connected card.
     * @param amount The amount to pay, which may not be negative. A zero amount is allowed, and can serve as a proof of
     *               transaction by means of the generated receipt.
     * @return A receipt of the transaction if the payment was successful, or {@link Optional#empty()} if the card did
     * not have sufficient balance.
     * @throws CardException if the card operation failed
     * @throws TerminalException if the card returned an unexpected response, or the MAC or signature failed to verify
     */
    public Optional<Receipt> pay(short amount) throws CardException, TerminalException {
		/* C-APDU: INS: PAY; Data:
			- Payment amount
			- Terminal nonce
			- MAC
		 */
        CommandAPDU commandAPDU = new CommandAPDUBuilder(Instruction.PAY, PAYMENT_RESPONSE_OK_APDU_LENGTH)
                .setExpectedLength(PAYMENT_COMMAND_APDU_LENGTH)
                .add(amount)
                .add(transactionId)
                .addMAC(macKey)
                .build();
        ResponseAPDU responseAPDU = channel.transmit(commandAPDU);
        checkSW(responseAPDU);
        checkDataLength(responseAPDU);

        byte[] data = responseAPDU.getData();
        boolean sufficientBalance = data[0] == 1;

        if(sufficientBalance) {
			/* R-APDU: OK;
				- Sufficient balance = '1'
				- Card nonce [1]
				- ECDSA signature by card on SIGN_PAYMENT_RECEIPT,
					terminal nonce, terminal ID, 1, payment amount
		 	*/
            // Verify card signature
            int signatureOffset = 1 + CARD_NONCE_LENGTH;
            byte[] signature = Arrays.copyOfRange(data, signatureOffset, signatureOffset + Crypto.SIGNATURE_LENGTH);
            byte[] cardNonce = Arrays.copyOfRange(data, 1, 1 + CARD_NONCE_LENGTH);

            if(!new SignatureBuilder()
                    .setExpectedLength(TERMINAL_NONCE_LENGTH + ID_LENGTH + CARD_NONCE_LENGTH + BALANCE_LENGTH)
                    .add(transactionId)
                    .add(terminalId)
                    .add(cardNonce)
                    .add(amount)
                    .verify(cardPublicKey, SignatureDomain.SIGN_PAYMENT_RECEIPT, signature)
            ) {
                throw new TerminalException("Failed to verify payment signature");
            }

            // Create terminal signature and receipt (proof of payment)
            Instant instant = Instant.now();
            byte[] terminalSignature = new SignatureBuilder()
                    .add(cardId)
                    .add(instant.getEpochSecond())
                    .add(signature)
                    .build(terminalKeyPair.getPrivate(), SignatureDomain.SIGN_PAYMENT_RECEIPT);
            Receipt receipt = new Receipt(cardId, cardPublicKey, cardNonce, signature,
                    terminalId, terminalKeyPair.getPublic(), transactionId, terminalSignature, amount, instant);

            // Increment transaction identifier once the payment as been fully accepted
            incrementTransactionId();

            return Optional.of(receipt);
        } else {
            // R-APDU: OK; sufficient balance = '0', MAC
            if(!Crypto.verifyMAC(macKey, Instruction.PAY, data)) {
                throw new TerminalException("Failed to verify MAC on PAY");
            }

            return Optional.empty();
        }
    }

}
