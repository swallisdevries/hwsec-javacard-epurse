package epurse.terminal;

import com.licel.jcardsim.utils.AIDUtil;
import javacard.framework.AID;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class Constants {
    public static final AID CALC_APPLET_AID = AIDUtil.create(new byte[]{0x3B, 0x29, 0x63, 0x61, 0x6C, 0x63, 0x01});
    public static final int TERMINAL_NONCE_LENGTH = 16;
    public static final int CARD_NONCE_LENGTH     = 4;
    public static final int ID_LENGTH = 4;
    public static final int BALANCE_LENGTH = 2;
    public static final int INIT_COMMAND_APDU_LENGTH = Crypto.PUBLIC_KEY_LENGTH + Crypto.KEY_PAIR_LENGTH + Crypto.SIGNATURE_LENGTH + ID_LENGTH + BALANCE_LENGTH;
    public static final int HS_1_COMMAND_APDU_LENGTH = Crypto.PUBLIC_KEY_LENGTH + ID_LENGTH + Crypto.SIGNATURE_LENGTH;
    public static final int HS_1_RESPONSE_APDU_LENGTH = Crypto.PUBLIC_KEY_LENGTH + ID_LENGTH + Crypto.SIGNATURE_LENGTH + CARD_NONCE_LENGTH;
    public static final int HS_2_COMMAND_APDU_LENGTH = Crypto.PUBLIC_KEY_LENGTH + Crypto.SIGNATURE_LENGTH + TERMINAL_NONCE_LENGTH;
    public static final int HS_2_RESPONSE_APDU_LENGTH = Crypto.PUBLIC_KEY_LENGTH + Crypto.SIGNATURE_LENGTH;
    public static final int BALANCE_COMMAND_APDU_LENGTH = Crypto.MAC_LENGTH;
    public static final int BALANCE_RESPONSE_APDU_LENGTH = BALANCE_LENGTH + Crypto.MAC_LENGTH;
    public static final int RELOAD_COMMAND_APDU_LENGTH = BALANCE_LENGTH + Crypto.MAC_LENGTH;
    public static final int RELOAD_RESPONSE_APDU_LENGTH = Crypto.MAC_LENGTH;
    public static final int REVOKE_COMMAND_APDU_LENGTH = Crypto.MAC_LENGTH;
    public static final int REVOKE_RESPONSE_APDU_LENGTH = Crypto.MAC_LENGTH;
    public static final int PAYMENT_COMMAND_APDU_LENGTH = BALANCE_LENGTH + TERMINAL_NONCE_LENGTH + Crypto.MAC_LENGTH;
    public static final int PAYMENT_RESPONSE_OK_APDU_LENGTH = 1 + CARD_NONCE_LENGTH + Crypto.SIGNATURE_LENGTH;
    public static final int PAYMENT_RESPONSE_FAIL_APDU_LENGTH = 1 + Crypto.MAC_LENGTH;
}
