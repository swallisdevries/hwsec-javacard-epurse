package epurse.terminal;

import javacard.security.ECPublicKey;
import javacard.security.PrivateKey;
import javacard.security.PublicKey;

import java.util.Arrays;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class SignatureBuilder {
    private byte[] data;
    private int offset;

    public SignatureBuilder() {
        this.data = null;
        this.offset = 0;
    }

    private void ensureAvailable(int bytes) {
        if(data == null) {
            data = new byte[bytes];
        } else if(data.length - offset < bytes) {
            data = Arrays.copyOfRange(data, 0, offset + bytes);
        }
    }

    public SignatureBuilder setExpectedLength(int length) {
        ensureAvailable(length);

        return this;
    }

    public SignatureBuilder add(PublicKey key) {
        if(!(key instanceof ECPublicKey)) {
            throw new IllegalArgumentException("key must be an ECPrivateKey");
        }

        ensureAvailable(Crypto.PUBLIC_KEY_LENGTH);
        offset += ((ECPublicKey)key).getW(data, (short)offset);

        return this;
    }

    public SignatureBuilder add(long value) {
        ensureAvailable(Long.BYTES);
        Util.packLong(value, data, offset);
        offset += Long.BYTES;

        return this;
    }

    public SignatureBuilder add(int value) {
        ensureAvailable(Integer.BYTES);
        Util.packInt(value, data, offset);
        offset += Integer.BYTES;

        return this;
    }

    public SignatureBuilder add(short value) {
        ensureAvailable(Short.BYTES);
        Util.packShort(value, data, offset);
        offset += Short.BYTES;

        return this;
    }

    public SignatureBuilder add(byte[] value) {
        return add(value, 0, value.length);
    }

    public SignatureBuilder add(byte[] value, int offset, int length) {
        ensureAvailable(length);
        System.arraycopy(value, offset, data, this.offset, length);
        this.offset += length;

        return this;
    }

    public byte[] build(PrivateKey key, byte domain) {
        return Crypto.generateSignature(key, data, 0, offset, domain);
    }

    public boolean verify(PublicKey key, byte domain, byte[] signature) {
        return Crypto.verifySignature(key, data, 0, offset, signature, domain);
    }
}
