package epurse.terminal;

import epurse.card.EPurseApplet;
import javacard.security.*;

import javax.smartcardio.CommandAPDU;
import java.util.Arrays;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class CommandAPDUBuilder {
    private final int cla;
    private final int ins;
    private final int p1;
    private final int p2;
    private final int ne;
    private byte[] data;
    private int offset;

    public CommandAPDUBuilder(int ins, int ne) {
        this(ins, ne, 0, 0);
    }

    public CommandAPDUBuilder(int ins, int ne, int p1, int p2) {
        this(ins, ne, p1, p2, EPurseApplet.CLASS);
    }

    public CommandAPDUBuilder(int ins, int ne, int p1, int p2, int cla) {
        this.cla = cla;
        this.ins = ins;
        this.p1 = p1;
        this.p2 = p2;
        this.ne = ne;
        this.data = null;
        this.offset = 0;
    }

    private void ensureAvailable(int bytes) {
        if(data == null) {
            data = new byte[bytes];
        } else if(data.length - offset < bytes) {
            data = Arrays.copyOfRange(data, 0, offset + bytes);
        }
    }

    public CommandAPDUBuilder setExpectedLength(int length) {
        ensureAvailable(length);

        return this;
    }

    public CommandAPDUBuilder add(PrivateKey key) {
        if(!(key instanceof ECPrivateKey)) {
            throw new IllegalArgumentException("key must be an ECPrivateKey");
        }

        ensureAvailable(Crypto.PRIVATE_KEY_LENGTH);
        offset += ((ECPrivateKey)key).getS(data, (short)offset);

        return this;
    }

    public CommandAPDUBuilder add(PublicKey key) {
        if(!(key instanceof ECPublicKey)) {
            throw new IllegalArgumentException("key must be an ECPrivateKey");
        }

        ensureAvailable(Crypto.PUBLIC_KEY_LENGTH);
        offset += ((ECPublicKey)key).getW(data, (short)offset);

        return this;
    }

    public CommandAPDUBuilder add(int value) {
        ensureAvailable(Integer.BYTES);
        Util.packInt(value, data, offset);
        offset += Integer.BYTES;

        return this;
    }

    public CommandAPDUBuilder add(short value) {
        ensureAvailable(Short.BYTES);
        Util.packShort(value, data, offset);
        offset += Short.BYTES;

        return this;
    }

    public CommandAPDUBuilder add(byte[] value) {
        return add(value, 0, value.length);
    }

    public CommandAPDUBuilder add(byte[] value, int offset, int length) {
        ensureAvailable(length);
        System.arraycopy(value, offset, data, this.offset, length);
        this.offset += length;

        return this;
    }

    public CommandAPDUBuilder addMAC(HMACKey key) {
        ensureAvailable(Crypto.MAC_LENGTH);
        Crypto.computeMAC(key, cla, ins, p1, p2, data, 0, offset, data, offset);
        offset += Crypto.MAC_LENGTH;

        return this;
    }

    public CommandAPDU build() {
        return new CommandAPDU(cla, ins, p1, p2, data, ne);
    }
}
