package epurse.terminal;

import epurse.card.Status;
import javacard.framework.ISO7816;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class TerminalException extends Exception {
    private static final Map<Short, String> ISO_EXCEPTIONS = new HashMap<>();

    static {
        ISO_EXCEPTIONS.put(ISO7816.SW_APPLET_SELECT_FAILED, "Applet selection failed");
        ISO_EXCEPTIONS.put(ISO7816.SW_BYTES_REMAINING_00, "Response bytes remaining = %d");
        ISO_EXCEPTIONS.put(ISO7816.SW_CLA_NOT_SUPPORTED, "CLA value not supported");
        ISO_EXCEPTIONS.put(ISO7816.SW_COMMAND_CHAINING_NOT_SUPPORTED, "Command chaining not supported");
        ISO_EXCEPTIONS.put(ISO7816.SW_COMMAND_NOT_ALLOWED, "Command not allowed (no current EF)");
        ISO_EXCEPTIONS.put(ISO7816.SW_CONDITIONS_NOT_SATISFIED, "Conditions of use not satisfied");
        ISO_EXCEPTIONS.put(ISO7816.SW_CORRECT_LENGTH_00, "Correct Expected Length (Le) = %d");
        ISO_EXCEPTIONS.put(ISO7816.SW_DATA_INVALID, "Data invalid");
        ISO_EXCEPTIONS.put(ISO7816.SW_FILE_FULL, "Not enough memory space in the file");
        ISO_EXCEPTIONS.put(ISO7816.SW_FILE_INVALID, "File invalid");
        ISO_EXCEPTIONS.put(ISO7816.SW_FILE_NOT_FOUND, "File not found");
        ISO_EXCEPTIONS.put(ISO7816.SW_FUNC_NOT_SUPPORTED, "Function not supported");
        ISO_EXCEPTIONS.put(ISO7816.SW_INCORRECT_P1P2, "Incorrect parameters (P1,P2)");
        ISO_EXCEPTIONS.put(ISO7816.SW_INS_NOT_SUPPORTED, "INS value not supported");
        ISO_EXCEPTIONS.put(ISO7816.SW_LAST_COMMAND_EXPECTED, "Last command in chain expected");
        ISO_EXCEPTIONS.put(ISO7816.SW_LOGICAL_CHANNEL_NOT_SUPPORTED, "Card does not support the operation on the specified logical channel");
        ISO_EXCEPTIONS.put(ISO7816.SW_NO_ERROR, "No Error");
        ISO_EXCEPTIONS.put(ISO7816.SW_RECORD_NOT_FOUND, "Record not found");
        ISO_EXCEPTIONS.put(ISO7816.SW_SECURE_MESSAGING_NOT_SUPPORTED, "Card does not support secure messaging");
        ISO_EXCEPTIONS.put(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED, "Security condition not satisfied");
        ISO_EXCEPTIONS.put(ISO7816.SW_UNKNOWN, "No precise diagnosis");
        ISO_EXCEPTIONS.put(ISO7816.SW_WARNING_STATE_UNCHANGED, "Warning, card state unchanged");
        ISO_EXCEPTIONS.put(ISO7816.SW_WRONG_DATA, "Wrong data");
        ISO_EXCEPTIONS.put(ISO7816.SW_WRONG_LENGTH, "Wrong length");
        ISO_EXCEPTIONS.put(ISO7816.SW_WRONG_P1P2, "Incorrect parameters (P1,P2)");
        ISO_EXCEPTIONS.put(Status.REVOKED, "The card has been revoked");
    }

    private static final List<Short> CUSTOMIZED_EXCEPTIONS = List.of(
            ISO7816.SW_BYTES_REMAINING_00,
            ISO7816.SW_CORRECT_LENGTH_00
    );

    public TerminalException(String message) {
        this(message, null);
    }

    public TerminalException(String message, Throwable cause) {
        super(message, cause);
    }

    public static TerminalException fromISOStatusWord(short sw) {
        short upper = (short)(sw & 0xff00);
        short lower = (short)(sw & 0x00ff);
        boolean isCustomized = CUSTOMIZED_EXCEPTIONS.contains(upper);

        // Customized status words store data in the lower byte, so mask it off to determine the status word type.
        if(isCustomized) {
            sw = upper;
        }

        String errorMessage = ISO_EXCEPTIONS.getOrDefault(sw, "Unknown ISO7816 status word");

        // Apply data from lower byte to create the customized error message.
        if(isCustomized) {
            errorMessage = String.format(errorMessage, lower);
        }

        return new TerminalException(String.format("Received status word (%04x): %s", sw, errorMessage));
    }
}
