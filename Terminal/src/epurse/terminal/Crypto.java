package epurse.terminal;

import epurse.card.EPurseApplet;
import javacard.security.*;

import java.security.SecureRandom;

/**
 * @author Ciske Harsema
 * @author Thijs Fransen
 */
public class Crypto {
    public static final int PUBLIC_KEY_LENGTH = 65;
    public static final int PRIVATE_KEY_LENGTH = 32;
    public static final int KEY_PAIR_LENGTH = PUBLIC_KEY_LENGTH + PRIVATE_KEY_LENGTH;
    public static final int SIGNATURE_LENGTH = 73;
    public static final int MAC_LENGTH = 32;

    /**
     * Generates a new random EC 256-bit FP key pair.
     * @return The generated key pair.
     */
    public static KeyPair generateKeyPair() {
        KeyPair keyPair = new KeyPair(KeyPair.ALG_EC_FP, KeyBuilder.LENGTH_EC_FP_256);

        keyPair.genKeyPair();

        return keyPair;
    }

    /**
     * Generates an issuer certificate over the provided data, which is used to authenticate terminals and cards as
     * being issued by the issuer.
     * @param issuerKeyPair The key pair of the issuer.
     * @param publicKey The public key to use in the certificate.
     * @param id The identifier to use in the certificate.
     * @param domain The certificate domain, as defined by {@link epurse.card.SignatureDomain}.
     * @return The generated certificate in a length prefix encoding.
     */
    public static byte[] generateIssuerCertificate(KeyPair issuerKeyPair, ECPublicKey publicKey, byte[] id, byte domain) {
        byte[] data = new byte[PUBLIC_KEY_LENGTH + id.length];

        short keyLength = publicKey.getW(data, (short)0);
        assert keyLength == PUBLIC_KEY_LENGTH;
        System.arraycopy(id, 0, data, PUBLIC_KEY_LENGTH, id.length);

        return generateSignature(issuerKeyPair.getPrivate(), data, 0, data.length, domain);
    }

    /**
     * Verifies the provided signature.
     * @param publicKey The public key used to verify the signature.
     * @param data The data which was signed.
     * @param offset The offset into the data array where the signature content data starts.
     * @param length The length of the signature content data.
     * @param signature The signature to verify, in a length prefix encoded format.
     * @param domain The signature domain, as defined by {@link epurse.card.SignatureDomain}.
     * @return True if the signature is valid, false otherwise.
     */
    public static boolean verifySignature(Key publicKey, byte[] data, int offset, int length, byte[] signature, byte domain) {
        Signature verifier = Signature.getInstance(Signature.ALG_ECDSA_SHA, false);
        verifier.init(publicKey, Signature.MODE_VERIFY);

        verifier.update(new byte[]{domain}, (short)0, (short)1);

        return verifier.verify(data, (short)offset, (short)length, signature, (short)1, signature[0]);
    }

    /**
     * Generates a signature over the provided data.
     * @param privateKey The private key to sign with.
     * @param data The data to sign.
     * @param offset The offset into the data array where the data to sign begins.
     * @param length The length of the data to sign.
     * @param domain The signature domain, as defined by {@link epurse.card.SignatureDomain}.
     * @return The generated signature in a length prefix encoding.
     */
    public static byte[] generateSignature(Key privateKey, byte[] data, int offset, int length, byte domain) {
        byte[] signature = new byte[SIGNATURE_LENGTH];

        Signature signer = Signature.getInstance(Signature.ALG_ECDSA_SHA, false);
        signer.init(privateKey, Signature.MODE_SIGN);

        signer.update(new byte[]{domain}, (short)0, (short)1);
        short signatureLength = signer.sign(data, (short)offset, (short)length, signature, (short)1);
        signature[0] = (byte)signatureLength;

        return signature;
    }

    /**
     * Generates a shared secret using the EC secret value derivation primitive, Diffie-Hellman version.
     * @param privateKey The private key used to establish the shared secret.
     * @param publicKey The public data, which is an EC public key, used to establish the shared secret.
     * @return The SHA-512 hash over the shared SVDP-DH data.
     */
    public static byte[] computeSharedSecret(PrivateKey privateKey, byte[] publicKey) {
        byte[] shared = new byte[256];
        byte[] hash = new byte[MessageDigest.LENGTH_SHA_512];

        KeyAgreement keyAgreement = KeyAgreement.getInstance(KeyAgreement.ALG_EC_SVDP_DH, false);
        keyAgreement.init(privateKey);
        short secretLength = keyAgreement.generateSecret(
                publicKey, (short)0, (short)Crypto.PUBLIC_KEY_LENGTH,
                shared, (short) 0);

        MessageDigest.getInstance(MessageDigest.ALG_SHA_512, false).doFinal(
                shared, (short) 0, secretLength,
                hash, (short) 0);

        return hash;
    }

    /**
     * Generates a random sequence of bytes of the provided length using a secure random generator.
     * @param length The number of random bytes to generate.
     * @return The generated random byte sequence.
     */
    public static byte[] getNonce(int length) {
        SecureRandom random = new SecureRandom();
        byte[] nonce = new byte[length];

        random.nextBytes(nonce);

        return nonce;
    }

    /**
     * Computes a MAC over the provided C-APDU message.
     * @param key The HMAC key used to compute the MAC.
     * @param cla The class byte of the C-APDU message.
     * @param ins The instruction byte of the C-APDU message.
     * @param p1 The p1 byte of the C-APDU message.
     * @param p2 The p2 byte of the C-APDU message.
     * @param data The user data of the C-APDU message.
     * @param offset The offset into the data array at which the user data begins.
     * @param length The length of the user data.
     * @param mac The destination array used to store the resulting MAC.
     * @param macOffset The offset into the destination array at which to begin storing the MAC.
     */
    public static void computeMAC(HMACKey key, int cla, int ins, int p1, int p2, byte[] data, int offset, int length, byte[] mac, int macOffset) {
        Signature signer = Signature.getInstance(Signature.ALG_HMAC_SHA_256, false);
        signer.init(key, Signature.MODE_SIGN);

        signer.update(new byte[]{(byte)cla, (byte)ins, (byte)p1, (byte)p2, (byte)(MAC_LENGTH + length - offset)}, (short)0, (short)5);
        short macLength = signer.sign(data, (short)offset, (short)length, mac, (short)macOffset);
        assert macLength == MAC_LENGTH;
    }

    /**
     * Verifies the MAC of the provided R-APDU message. This MAC includes the mandatory header of the corresponding
     * C-APDU message. The p1 and p2 parameters are always assumed to be zero, and the class byte is assumed to be equal
     * to {@link EPurseApplet#CLASS}. Only the instruction byte can be specified.
     * @param key The HMAC key used to verify the MAC.
     * @param instruction The instruction byte of the corresponding C-APDU message.
     * @param data The byte array containing the response data over which the MAC was computed, followed by the expected
     *             MAC itself.
     * @return True if the MAC is valid, false otherwise.
     */
    public static boolean verifyMAC(HMACKey key, byte instruction, byte[] data) {
        Signature signer = Signature.getInstance(Signature.ALG_HMAC_SHA_256, false);
        signer.init(key, Signature.MODE_VERIFY);

        short macOffset = (short)(data.length - MAC_LENGTH);

        signer.update(new byte[]{EPurseApplet.CLASS, instruction, (byte)0, (byte)0}, (short)0, (short)4);
        return signer.verify(data, (short)0, macOffset, data, macOffset, (short)MAC_LENGTH);
    }

    /**
     * Creates a fingerprint string of the provided public key by calling {@link #fingerprint(byte[])} on the key data.
     * @param key The public key, which must be an {@link ECPublicKey} instance.
     * @return The fingerprint hex string.
     */
    public static String fingerprint(PublicKey key) {
        if (!(key instanceof ECPublicKey)) {
            throw new IllegalArgumentException("key must be an ECPublicKey");
        }

        byte[] data = new byte[PUBLIC_KEY_LENGTH];
        ((ECPublicKey) key).getW(data, (short) 0);

        return fingerprint(data);
    }

    /**
     * Creates a fingerprint string of the provided data by applying a SHA-256 hash, which is then shortened by XOR-ing
     * the high nibble of each byte with the low nibble of that same byte, which produces a single hex character.
     * @param data The data to fingerprint.
     * @return The fingerprint hex string.
     */
    public static String fingerprint(byte[] data) {
        byte[] hash = new byte[MessageDigest.LENGTH_SHA_256];

        MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false)
                .doFinal(data, (short) 0, (short) data.length, hash, (short) 0);

        StringBuilder stringBuilder = new StringBuilder(hash.length);
        for (byte b : hash) {
            int digit = (((int)b >> 4) & 0xf) ^ ((int)b & 0xf);
            char c = Character.forDigit(digit, 16);
            stringBuilder.append(c);
        }

        return stringBuilder.toString();
    }

}
