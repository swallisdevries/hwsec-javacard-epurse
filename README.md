Execute `git submodule update --init` to download jCardSim.

Click on the run symbol in IntelliJ to run the terminal.

Useful references:
- [JavaCard 3.0.5 platform][1] ([3.1][6])
- Terminal:
  - [jCardSim][3] (also contains JavaCard platform docs)
  - [Java 11 Smart Card I/O][2]
  - [Regular Java 11 platform][4]
- Or just use <kbd>ctrl</kbd>+<kbd>Q</kbd> on a symbol :D
- [ISO 7816-4][5]

If you use IntelliJ, you first have to set up the project SDK to JDK 11 or newer.
If you just want to run it from the command line, run `./run.sh`.

[1]: https://docs.oracle.com/javacard/3.0.5/api/index.html
[2]: https://docs.oracle.com/en/java/javase/11/docs/api/java.smartcardio/javax/smartcardio/package-summary.html
[3]: https://jcardsim.org/jcardsim/
[4]: https://docs.oracle.com/en/java/javase/11/docs/api/
[5]: https://cardwerk.com/iso-7816-part-4
[6]: https://docs.oracle.com/en/java/javacard/3.1/jc_api_srvc/api_classic/