. envvars.sh

if [ "$OSTYPE" == "cygwin" ] || [ "$OSTYPE" == "msys" ]
then
  pathSep=";"
else
  pathSep=":"
fi

javac -Xlint:-options -source 7 -target 7 -bootclasspath "$jCardSimJar" $appletSource -d "$appletOutDir"
javac --release 11 -classpath "$jCardSimJar$pathSep$appletOutDir" -d "$terminalOutDir" $terminalSource

java -classpath "$jCardSimJar$pathSep$appletOutDir$pathSep$terminalOutDir" epurse.terminal.Main
