# Initialization terminal
C-APDU: INS: INITIALIZE; Data:
- (65) Issuer ECDSA public key
- (32) Card ECDSA secret key
- (65) Card ECDSA public key [1]
- (4)  Card ID [2]
- (73) ECDSA signature by issuer on CERT_CARD, 1-2
- (2)  Initial balance

# Other terminal
## MACs
For C-APDU: CLA, INS, P1, P2, Lc, Data (excl MAC)

For R-APDU: CLA, INS, P1, P2, response Data (NOTE: does not work when sending multiple messages with same INS etc!)

Length: 32

## Handshake
C-APDU: INS: HANDSHAKE, P1: 0, P2: terminal type; Data:
- (65) Terminal ECDSA public key [1]
- (4)  Terminal ID [2]
- (73) ECDSA signature by issuer on CERT_[RELOAD/PAYMENT], 1-2

R-APDU: OK;
- (65) Card ECDSA public key [1]
- (4)  Card ID [2]
- (73) ECDSA signature by issuer on CERT_CARD, 1-2
- (4)  Card nonce (big endian)

C-APDU: INS: HANDSHAKE, P1: 1; Data:
- (65) Terminal ECDHE public key [1]
- (73) ECDSA signature by terminal on SIGN_HANDSHAKE,
    ECDSA public key of card, card nonce, 1
- (4)  Terminal nonce
                           
R-APDU: OK;
- (65) Card ECDHE public key
- (73) ECDSA signature by card on SIGN_HANDSHAKE,
    ECDSA public key of terminal, terminal nonce, 1

### Revocation
C-APDU: INS: REVOKE; Data: (32) MAC

R-APDU: OK; (32) MAC

## Payment
C-APDU: INS: PAY; Data:
- (2)  Payment amount
- (16) Terminal nonce
- (32) MAC

**One of:**

R-APDU: OK;
- (1)  Sufficient balance = '1'
- (4)  Card nonce [1]
- (73) ECDSA signature by card on SIGN_PAYMENT_RECEIPT,
  terminal nonce, terminal ID, 1, payment amount

R-APDU: OK;
- (1)  Sufficient balance = '0'
- (32) MAC

## Give balance
C-APDU: INS: GIVE_BALANCE; Data: (32) MAC

R-APDU: OK;
- (2)  Card balance
- (32) MAC

## Reload
C-APDU: INS: RELOAD; Data:
- (2)  Increase balance amount
- (32) MAC

R-APDU: OK; (32) MAC
