#!/usr/bin/bash

export appletPackage=epurse.card
export appletClassName=EPurseApplet
export appletSource="CardApplet/src/epurse/card/*.java"
export jCardSimJar=jcardsim/jcardsim-3.0.4-SNAPSHOT.jar
export appletOutDir=out/production/CardApplet

export jcToolsHome=javacard-tools
export packageAid=0x3B:0x29:0x63:0x61:0x6C
export classAid=0x3B:0x29:0x63:0x61:0x6C:0x63:0x01
export appletVersion="1.0"

export terminalSource="Terminal/src/epurse/terminal/*.java Terminal/src/epurse/terminal/terminals/*.java"
export terminalOutDir=out/production/Terminal